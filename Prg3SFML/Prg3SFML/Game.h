#ifndef _GAME_H_
#define _GAME_H_
#include <SFML/Graphics.hpp>
#include <iostream>
#include <SFML/Audio.hpp>
#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include "PlayState.h"
#include "MenuState.h"
#include "Files.h"
#include "Clima.h"
#include "CreditsState.h"

using namespace std;
using namespace sf;

class  Game {

public:
	Game();
	~Game();
	bool StartUp();
	void MenuLoop();
	void Loop();
	bool Shutdown();
	


protected:
	SoundBuffer buffer;
	Sound sound;
	RenderWindow* window;
	PlayState Play;
	MenuState Menu;
	Files file;
	Clima clima;
	CreditsState* credits;
	
	
};
#endif