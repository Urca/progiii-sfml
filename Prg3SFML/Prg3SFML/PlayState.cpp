#include "..\Prg3SFML\PlayState.h"


bool PlayState::OnInit(RenderWindow *window) {
	
	if (!buffer.loadFromFile("pool.wav"))
	{
		cout << "Pool not loaded";
	}
		sound.setBuffer(buffer);

	clock = new Clock();
	elapsed = new Time();
	lives = new Lives(window);
	m = new Monedas(window);
	pj1->loadTexture("Personaje1.png");
	pj1->setPos(80, 80);
	en1->loadTexture("Enemigo1.png");
	en1->setPos(window->getSize().x/2, window->getSize().y/2);
	en1->setVel(25.0f);
	pj1->setVel(300.0f);
	m->loadTexture("Moneda1.png");
	m->setPos(rand() % window->getSize().x, rand() % window->getSize().y);

	for (int i = 0; i < 3; i++)
	{
		bombs[i] = new Enemy();
		bombs[i]->setPos(rand() % window->getSize().x, rand() % window->getSize().y);
		bombs[i]->loadTexture("Bomba1.png");
	}
	
	
	return true;
	
	

}

bool PlayState::OnUpdate(RenderWindow *window) {
	
	
		*elapsed = clock->restart();


		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			pj1->moveRight(elapsed);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			pj1->moveLeft(elapsed);
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			pj1->moveDown(elapsed);
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			pj1->moveUp(elapsed);
		}

	
		if (lives->getLives() <= 0)
		{
			_playing = false;
		}
		
		
		en1->eneMove(elapsed);
		
		if (pj1->getSprite().getGlobalBounds().intersects(en1->getSprite().getGlobalBounds()))
		{
			sound.play();
			pj1->setPos(80, 80);
			cout << "Yep...collision" << endl;
			lives->loseLife();
		}

		for (int i = 0; i < 3; i++)
		{
			if (pj1->getSprite().getGlobalBounds().intersects(bombs[i]->getSprite().getGlobalBounds()))
			{
				sound.play();
				pj1->setPos(80, 80);
				cout << "Yep...collision" << endl;
				lives->loseLife();
			}
		}

		if (pj1->getSprite().getGlobalBounds().intersects(m->getSprite().getGlobalBounds()))
		{
			m->setPos(rand() % window->getSize().x, rand() % window->getSize().y);
			cout << "Punto!" << endl;
			m->punto();
			m->intToString();
			cout << m->getPuntos() << endl;
			
		}


		return true;

	

	
}

bool PlayState::OnDraw(RenderWindow *window) {
	
	window->draw(pj1->getSprite());
	window->draw(en1->getSprite());
	window->draw(lives->getTextLives());
		for (int i = 0; i < lives->getLives(); i++)
	{
		window->draw(lives->getLife(i));
	}

	for (int i = 0; i < 3; i++)
	{
		window->draw(bombs[i]->getSprite());
	}
	window->draw(m->getSprite());
	window->draw(m->getTextPuntos());
	window->draw(m->getScore());
	window->draw(lives->getLife(0));
	window->display();
	

	

	return true;

}

bool PlayState::OnShutdown() 
{
	lives->~Lives();
	pj1->~Personaje();
	
	en1->~Enemy();
	m->~Monedas();
	for (int i = 0; i < 3; i++)
	{
		bombs[i]->~Enemy();
	}
	lives->~Lives();
	
	return true;
}




PlayState::PlayState() {

}
PlayState::~PlayState() {

}

