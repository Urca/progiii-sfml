#ifndef _PLAYSTATE_H_
#define _PLAYSTATE_H_
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Personaje.h"
#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include "Enemy.h"
#include "..\Prg3SFML\Lives.h"
#include <SFML/Audio.hpp>
#include "..\Prg3SFML\Monedas.h"


using namespace sf;
using namespace std;


class PlayState {
public:
	PlayState(/*StateManager *st*/);
	~PlayState();
	bool OnInit(RenderWindow *window);
	bool OnUpdate(RenderWindow *window);
	bool OnDraw(RenderWindow *Window);
	int random_number = std::rand();
	int getPuntos() { return m->getPuntos(); }
	int getlives() { return lives->getLives(); }
	bool getPlaying() { return _playing; }
	void setPlaying(bool playing) { _playing = playing; }
	bool OnShutdown();
	
protected:
	bool _playing = false;
	Personaje *pj1 = new Personaje;
	Monedas *m;
	//Personaje *pj2 = new Personaje;	
	Enemy *en1 = new Enemy;
	Enemy *bombs[3];
	Lives* lives;
	Clock* clock;
	Time* elapsed;
	Font font;
	Text textVidas;
	SoundBuffer buffer;
	sf::Sound sound;
	
};
#endif
