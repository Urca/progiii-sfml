#ifndef  _CLIMA_H_
#define _CLIMA_H_
#include <sfml/Network.hpp>
#include <iostream>
#include "document.h"
#include "writer.h"
#include "stringbuffer.h"
using namespace sf;
using namespace std;
using namespace rapidjson;

class Clima
{
public:
	Clima();
	~Clima();


protected:
	string clima;

public:
	
	void getClima();

	string getClimaString() { return clima; }
	
	
		
	
};


#endif // ! _CLIMA_H_
