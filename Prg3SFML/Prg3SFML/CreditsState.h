#ifndef  _CREDITS_STATE_H_
#define _CREDITS_STATE_H_
#include <SFML/Graphics.hpp>
#include <iostream>
#include <SFML\Window.hpp>
using namespace sf;

class CreditsState
{
public:
	CreditsState(RenderWindow* window);
	~CreditsState();
	bool OnDraw(RenderWindow* window);
	
	

protected:
	Font font;
	Text credits;
};
#endif // ! _CREDITS_STATE_H_

