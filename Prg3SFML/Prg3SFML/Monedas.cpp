#include "Monedas.h"



Monedas::Monedas(RenderWindow* window)
{


	if (!font.loadFromFile("arial.ttf"))
	{
		std::cout << "No se carga la fuente de los puntos";
	}

	textPuntos.setFont(font);
	textPuntos.setFillColor(sf::Color::Red);
	textPuntos.setString("Puntos");
	textPuntos.setPosition(window->getSize().x - 1000, window->getSize().y - 760);

	if (!fontScore.loadFromFile("arial.ttf"))
	{
		std::cout << "No se carga la fuente de los puntos";
	}
	ss << _puntos;
	score.setFont(font);
	score.setFillColor(sf::Color::Red);
	score.setString(ss.str());
	score.setPosition(window->getSize().x - 900, window->getSize().y - 760);
}


Monedas::~Monedas()
{
}

void Monedas::punto()
{
	_puntos++;
	score.setString(ss.str());

}

void Monedas::intToString() {
	ss << _puntos;
}


void Monedas::loadTexture(string tex)
{

	if (!texture.loadFromFile(tex))
	{
		cout << "No carga la textura del enemigo" << endl;

	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

void Monedas::setPos(float x, float y)
{
	sprite.setPosition(sf::Vector2f(x, y));

}

Sprite Monedas::getSprite()
{
	return sprite;
}

