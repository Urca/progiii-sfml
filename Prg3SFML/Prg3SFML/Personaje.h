#ifndef _PERSONAJE_H
#define _PERSONAJE_H_
#define vidas 3;
#include <SFML/Graphics.hpp>
#include <iostream>
using namespace std;
using namespace sf;

class Personaje
{
private:
	Texture texture;
	Sprite sprite;
	float _vel;
public:
	Personaje();
	~Personaje(); 
	sf::Sprite getSprite();
	void loadTexture(string tex);
	void moveRight(Time* elapsed);
	void moveLeft(Time* elapsed);
	void moveUp(Time* elapsed);
	void moveDown(Time* elapsed);
	void setPos(float x, float y);
	void getPo();
	void setVel(float vel);
	
	
};
#endif // !_PERSONAJE_H