#include "Clima.h"



Clima::Clima()
{
}


Clima::~Clima()
{
}

void Clima::getClima() {
	sf::Http http;
	http.setHost("http://query.yahooapis.com");
	StringBuffer buffer;
	Writer<StringBuffer> writer(buffer);
	sf::Http::Request request;
	request.setUri("/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22Buenos%20Aires%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
	sf::Http::Response response = http.sendRequest(request);
	string s = response.getBody();
	buffer.Clear();


	const char* json = s.c_str();
	printf("Original JSON:\n %s\n", json);
	Document d;
	d.Parse(json);
	d.Accept(writer);
	assert(d.IsObject());
	//assert(d.HasMember("query")("count");
	assert(d["query"]["results"]["channel"]["item"]["condition"]["text"].IsString());
	printf("Text = %s\n", d["query"]["results"]["channel"]["item"]["condition"]["text"].GetString());

	clima = buffer.GetString();
}
