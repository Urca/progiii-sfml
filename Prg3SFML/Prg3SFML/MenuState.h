#pragma once
#ifndef _MENU_STATE_H_
#define _MENU_STATE_H_
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Menu.h"
#include <SFML\Window.hpp>
using namespace sf;

class MenuState {
public:
	MenuState();
	~MenuState();
	bool OnInit(RenderWindow* menuWindow);
	bool OnUpdate();
	bool OnDraw(RenderWindow* menuWindow);
	bool OnShutdown();
	void MoveUp();
	void MoveDown();
	int GetPressedItem();


protected:

	Menu *menu;
	

};

#endif
