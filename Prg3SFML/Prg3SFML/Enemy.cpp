#include "..\Prg3SFML\Enemy.h"


Enemy::Enemy() {

}

Enemy::~Enemy() {

}
void Enemy::loadTexture(string tex)
{

	if (!texture.loadFromFile(tex))
	{
		cout << "No carga la textura del enemigo" << endl;

	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}
sf::Sprite Enemy::getSprite()
{
	return sprite;
}

void Enemy::setPos(float x, float y)
{
	sprite.setPosition(sf::Vector2f(x, y));

}

void Enemy::eneMove(Time* elapsed) {
	elapsedcount += elapsed->asSeconds();
	if (elapsedcount > 1.0f)
	{
		
		x = (rand() % 10)-5;
		y = (rand() % 10) -5;
		elapsedcount = 0;
	}
	

	sprite.move(x*elapsed->asSeconds()*_vel, y*elapsed->asSeconds()*_vel);
}