#ifndef _LIVES_H_
#define _LIVES_H_
#include "SFML/Graphics.hpp"
#define maxlife 3
#include <iostream>
using namespace std;
using namespace sf;

class Lives
{

public:
	Lives(RenderWindow* window);
	~Lives();

	
	Text getTextLives() { return textLives; }
	Sprite getLife(int lives) { return life[lives]; }
	int getLives() { return lives; }
	void loseLife();



protected:
	Texture texture;
	Sprite sprite;
	sf::Font font;
	sf::Text textLives;
	sf::Sprite life[maxlife];
	int lives=3;
};
#endif
