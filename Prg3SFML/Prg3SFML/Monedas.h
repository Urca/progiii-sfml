#ifndef _MONEDAS_H_
#define _MONEDAS_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include <sstream>
using namespace sf;
using namespace std;

class Monedas
{
protected:
	Texture texture;
	Sprite sprite;	
	Text textPuntos;
	Font font;
	Font fontScore;
	Text score;
	int _puntos = 0;
	stringstream ss;
	
public:
	Monedas(RenderWindow* window);
	~Monedas();
	Text getTextPuntos() { return textPuntos; }
	Text getScore() { return score; }
	void loadTexture(string tex);
	void setPos(float x, float y);
	Sprite getSprite();
	int getPuntos() { return _puntos; }
	void setPuntos(int puntos) { _puntos = puntos; }
	void punto();
	void intToString();
	void draw(RenderWindow* menuWindow);
};


#endif 
