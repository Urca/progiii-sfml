#ifndef _ENEMY_H_
#define _ENEMY_H_
#include <SFML/Graphics.hpp>
#include <iostream>
using namespace std;
using namespace sf;

class Enemy
{
private:
	Texture texture;
	Sprite sprite;
	float _vel;
public:
	float elapsedcount = 0;
	int x, y;
	Enemy();
	~Enemy();
	Sprite getSprite();
	void loadTexture(string tex);
	void setPos(float x, float y);

	void eneMove(Time* elapsed);
	void setVel(float vel) { _vel = vel; }


};
#endif // !_ENEMY_H_