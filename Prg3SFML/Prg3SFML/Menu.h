#ifndef _MENU_H_
#define _MENU_H_
#include "SFML/Graphics.hpp"
#define MAX_ITEMS 3
#include <iostream>
using namespace sf;

class Menu
{

public:
	Menu(float width, float height);
	~Menu();

	void draw(RenderWindow* menuWindow);
	void Moveup();
	void MoveDown();
	int GetPressedItem() { return selectedItemIndex; }
	

protected:
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[MAX_ITEMS];
};
#endif