#include "MenuState.h"


#include "..\Prg3SFML\PlayState.h"


bool MenuState::OnInit(RenderWindow* menuWindow) {

	menu = new Menu(menuWindow->getSize().x, menuWindow->getSize().y);
	return true;



}

bool MenuState::OnUpdate() {


	return true;

}

bool MenuState::OnDraw(RenderWindow* menuWindow) {
	menuWindow->clear(sf::Color::Black);
	menu->draw(menuWindow);
	menuWindow->display();




	return true;

}

bool MenuState::OnShutdown() {
	menu->~Menu();
	return true;
}

void MenuState::MoveUp()
{
	menu->Moveup();
}
void MenuState::MoveDown() 
{
	menu->MoveDown();
}

int MenuState::GetPressedItem()
{
	return menu->GetPressedItem();
}

MenuState::MenuState() {

}
MenuState::~MenuState() {

}