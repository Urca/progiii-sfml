#include "..\Prg3SFML\Game.h"


Game::Game() {

}
Game::~Game()
{

}
bool Game::StartUp()
{


	window = new RenderWindow(VideoMode(1024, 768), "Hola");
	credits = new CreditsState(window);
	if (!buffer.loadFromFile("RSB.wav"))
	{
		cout << "Error al cargar musica";
	}
	sound.setBuffer(buffer);
	Menu.OnInit(window);
	Play.OnInit(window);
	file.openFile();
	//file.readFile();
	clima.getClima();
	return true;


}

void Game::MenuLoop()
{

}

void Game::Loop()
{
	file.escribirpuntos();
	while (window->isOpen())
	{
		sf::Event event;

		while (window->pollEvent(event))
		{

			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Up:
					Menu.MoveUp();
					break;
				case sf::Keyboard::Down:
					Menu.MoveDown();
					break;
				case sf::Keyboard::Return:
					switch (Menu.GetPressedItem())
					{
					case 0:
						cout << "Play" << std::endl;
						Play.setPlaying(true);

						sound.play();
						while (Play.getPlaying() == true)
						{
							if (clima.getClimaString().compare("Cloudy") || clima.getClimaString().compare("Mostly Cloudy"))
							{
								window->clear(sf::Color::Blue);
							}
							else if (clima.getClimaString().compare("Clear") || clima.getClimaString().compare("Mostly Clear"))
							{
								window->clear(sf::Color::Cyan);
							}
							else if (clima.getClimaString().compare("Sunny") || clima.getClimaString().compare("Mostly Sunny"))
							{
								window->clear(sf::Color::Yellow);
							}
							else
							{
								window->clear(sf::Color::White);
							}

							Play.OnDraw(window);
							Play.OnUpdate(window);

							

						}


						//file.writeFile(Play.getPuntos());


						break;
					case 1:
						sound.stop();
						while (Play.getPlaying() == false)
						{
							window->clear(sf::Color::Black);
							credits->OnDraw(window);

						}

						cout << "Credits" << std::endl;

						break;
					case 2:cout << "Exit" << std::endl;
						window->close();
						break;

					}

				}
				break;

			case sf::Event::Closed:
				window->close();
				break;
			}


		}
		Menu.OnDraw(window);
	}







}









bool Game::Shutdown() {

	file.writeFile(Play.getPuntos());
	Play.OnShutdown();
	Menu.OnShutdown();
	Menu.~MenuState();
	Play.~PlayState();
	credits->~CreditsState();
	file.closeFile();
	file.closeReader();
	return true;
}

